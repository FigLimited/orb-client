
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue'), name: 'index' },
      { path: 'play', component: () => import('pages/Play.vue'), name: 'play' },
      { path: 'login', component: () => import('pages/Login.vue'), name: 'login' },
      { path: 'register', component: () => import('pages/Register.vue'), name: 'register' }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
