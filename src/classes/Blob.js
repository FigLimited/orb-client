export class Blob {
  constructor (p5, x, y, radius, range, id) {
    this.p5 = p5
    this.pos = p5.createVector(x, y)
    this.velocity = p5.createVector(0, 0)
    this.radius = radius
    this.range = range
    this.id = id
  }

  update (x, y) {
    var newVelocity = this.p5.createVector(x, y)
    newVelocity.setMag(2)
    this.velocity.lerp(newVelocity, 0.5)
    this.pos.add(this.velocity)
  }

  show (me) {
    var fill = me ? 255 : 0
    this.p5.fill(fill)
    this.p5.ellipse(this.pos.x, this.pos.y, this.r * 2, this.r * 2)
    this.p5.fill(fill)
    this.p5.textAlign(this.p5.CENTER, this.p5.CENTER)
    this.p5.text(this.id, this.pos.x, this.pos.y + this.radius + 10)
  }
}

// Blob.prototype.eats = function (other) {
//   if (this.range(other)) {
//     var sum = Math.PI * this.r * this.r + Math.PI * other.r * other.r
//     this.r = Math.sqrt(sum / Math.PI)
//     // this.r += other.r * 0.2
//     return true
//   } else {
//     return false
//   }
// }

Blob.prototype.range = function (other) {
  //  if centers are closer than both radii added together then hit
  var x = Math.abs(this.pos.x - other.pos.x)
  var y = Math.abs(this.pos.y - other.pos.y)
  var h = Math.sqrt(x * x + y * y)
  // console.log(h, this.r, other.r, this.r + other.r <= h)
  if (this.r + other.r >= h) {
    return true
  } else {
    return false
  }
}

Blob.prototype.constrain = function (w, h) {
  this.pos.x = this.sk.constrain(this.pos.x, -w / 2, w / 2)
  this.pos.y = this.sk.constrain(this.pos.y, -h / 2, h / 2)
}
