export default class Line {
  constructor (me, other, h) {
    this.me = me
    this.other = other
    this.h = h
  }

  show (p5) {
    p5.beginShape()
    p5.strokeWeight(5)
    p5.stroke(236, 15, 19)
    for (var j = 0; j < this.h; j += 5) {
      var x1 = p5.map(j, 0, this.h, Math.floor(this.me.pos.x), Math.floor(this.other.pos.x))
      var y1 = p5.map(j, 0, this.h, Math.floor(this.me.pos.y), Math.floor(this.other.pos.y))
      p5.vertex(x1 + p5.random(-10, 10), y1 + p5.random(-10, 10))
      // console.log(x, y)
    }
    p5.endShape(p5.CLOSE)
    p5.beginShape()
    p5.strokeWeight(3)
    p5.stroke(225, 215, 255)
    for (var k = 0; k < this.h; k += 20) {
      var x2 = p5.map(k, 0, this.h, Math.floor(this.me.pos.x), Math.floor(this.other.pos.x))
      var y2 = p5.map(k, 0, this.h, Math.floor(this.me.pos.y), Math.floor(this.other.pos.y))
      p5.vertex(x2 + p5.random(-15, 15), y2 + p5.random(-15, 15))
      // console.log(x, y)
    }
    p5.endShape(p5.CLOSE)
  }
}
