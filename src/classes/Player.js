import Line from '../classes/Line'

export default class Player {
  id
  name
  pos
  radius
  arcRadius
  team
  velocity
  power
  score
  playersInRange = []
  maxVelocity
  constructor (p5, x, y, r, a, id, n, team) {
    this.id = id
    this.name = n
    this.pos = p5.createVector(x, y)
    this.radius = r
    this.arcRadius = a
    this.team = team
    this.velocity = p5.createVector(0, 0)
    this.maxVelocity = 3
    this.power = 0
    this.score = 0
  }

  move (p5, g, canvas) {
    var x = 0
    var y = 0
    if (p5.keyIsDown(p5.LEFT_ARROW)) {
      x = -5 * (g.deltaTime / 50)
    }
    if (p5.keyIsDown(p5.RIGHT_ARROW)) {
      x = 5 * (g.deltaTime / 50)
    }
    if (p5.keyIsDown(p5.UP_ARROW)) {
      y = -5 * (g.deltaTime / 50)
    }
    if (p5.keyIsDown(p5.DOWN_ARROW)) {
      y = 5 * (g.deltaTime / 50)
    }
    var newVelocity = p5.createVector(x, y)
    newVelocity.setMag(this.maxVelocity)
    this.velocity.lerp(newVelocity, 0.1)
    this.pos.add(this.velocity)
    this.pos.x = p5.constrain(this.pos.x, 0, g.width)
    this.pos.y = p5.constrain(this.pos.y, 0, g.height)
  }

  update (p5, x, y, g) {
    var newVelocity = p5.createVector(x - p5.width / 2, y - p5.height / 2)
    newVelocity.setMag(this.maxVelocity)
    this.velocity.lerp(newVelocity, 0.1)
    this.pos.add(this.velocity)
    this.pos.x = p5.constrain(this.pos.x, 0, g.width)
    this.pos.y = p5.constrain(this.pos.y, 0, g.height)
  }

  show (p5, me, g, c, playersOnScreen = []) {
    if (me) {
      //  draw the aura regardless
      p5.stroke(0)
      p5.strokeWeight(1)
      p5.noFill()
      p5.ellipse(this.pos.x, this.pos.y, this.arcRadius * 2, this.arcRadius * 2)
    }
    //  check if hitting anyone
    playersOnScreen.forEach(element => {
      let radii = (this.arcRadius + element.radius) * (this.arcRadius + element.radius)
      let x = Math.abs(this.pos.x - element.pos.x)
      let y = Math.abs(this.pos.y - element.pos.y)
      var hh = x * x + y * y

      if (radii >= hh) {
        var h = p5.sqrt(hh)
        let l = new Line(this, element, h)
        l.show(p5)
      }
    })
    //  draw the player
    p5.fill(this.team.hex)
    p5.strokeWeight(3)
    p5.ellipse(this.pos.x, this.pos.y, this.radius * 2, this.radius * 2)
    p5.strokeWeight(0)
    p5.fill(255, 0, 20)
    p5.textAlign(p5.CENTER, p5.CENTER)
    p5.textSize(24)
    p5.text(this.name, this.pos.x, this.pos.y + this.radius + 12)
    p5.text(Math.floor(this.score), this.pos.x, this.pos.y + this.radius + 40)
  }

  range (other, maxRadius) {
    // console.log(other)
    //  if centers are closer than both radii added together then hit
    var x = Math.abs(this.pos.x - other.pos.x)
    var y = Math.abs(this.pos.y - other.pos.y)
    var hh = x * x + y * y
    var radii = (this.radius + other.radius) * (this.radius + other.radius)
    if (radii >= hh) {
      //  add a percentage of max range and this range
      var d = maxRadius - this.radius
      var p = d * 0.001
      this.radius += p
      this.score += other.radius
      this.power += other.radius
      // this.r += other.r * 0.2
      return true
    } else {
      return false
    }
  }
}
