export default class Spark {
  constructor (p5, x, y, r, id) {
    this.pos = p5.createVector(x, y)
    this.id = id
    this.radius = r
  }

  show (p5) {
    p5.fill(200, 100, 50)
    p5.ellipse(this.pos.x, this.pos.y, this.radius * 2, this.radius * 2)
    p5.fill(0, 20, 200)
    p5.textAlign(p5.CENTER, p5.CENTER)
    p5.textSize(18)
    // p5.text(this.pos.x + ', ' + this.pos.y, this.pos.x, this.pos.y + this.radius + 12)
    p5.text(this.id, this.pos.x, this.pos.y + this.radius + 12)
  }
}
