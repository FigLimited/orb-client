// import something here
import socketio from 'socket.io-client'

// "async" is optional
export default async ({ Vue }) => {
  // something to do
  Vue.prototype.$socket = socketio(process.env.API)
}
